package trackrecon.dataflow

import com.spotify.scio._
import com.spotify.scio.util._

import java.net.URI

import com.github.tototoshi.csv._

import com.google.api.services.bigquery.model.TableRow

import org.apache.beam.sdk.io.gcp.bigquery.BigQueryIO

import org.slf4j.LoggerFactory


object ParticlesToBigQuery {

  val particles_columns = List(
    "particle_id",
    "vx","vy","vz",
    "px","py","pz",
    "q","nhits")

  val rem = new RemoteFileUtil()

  private val logger = LoggerFactory.getLogger(this.getClass)

  // BigQuery spits any non-JSON compliant entry out; so we filter NaNs and Infs
  def safe_set(t: TableRow, col: (String, String)) = {
    try {
      val entry = col._2.toDouble
      if (entry.isNaN || entry.isInfinity) {
        logger.warn("A NaN or INF entry was dropped.")
        t
      } else t.set(col._1, col._2)
    } catch {
      case _ : Throwable => t
    }
  }

  def main(cmdlineargs: Array[String]) = {
    val (sc, args) = ContextAndArgs(cmdlineargs)

    sc.parallelize(1000 to 10000)
      .flatMap {e => {
        val particles_uri = new URI("gs://trackrecon-204713/vanilla/event00000" + e + "-particles.csv")
        if (rem.remoteExists(particles_uri)) {
          val local_path = rem.download(particles_uri)
          val reader = CSVReader.open(local_path.toFile())
          val data = reader.all().drop(1)
          reader.close()
          List((e, data.map(_.toIndexedSeq)))
        } else List.empty
      }}
      .map { case (e, pps) => {
        (e, pps.map(particles_columns zip _))
      }}
      .flatMap { case (e, ppst) => {
        for (pt <- ppst) yield {
          val trow = pt.foldLeft(new TableRow())(safe_set)
          trow.set("event_id", e.toString)
        }
      }}
      .saveAsBigQuery("train.particles",
        createDisposition=BigQueryIO.Write.CreateDisposition.CREATE_NEVER,
        writeDisposition=BigQueryIO.Write.WriteDisposition.WRITE_TRUNCATE)

    sc.close()
  }

}
