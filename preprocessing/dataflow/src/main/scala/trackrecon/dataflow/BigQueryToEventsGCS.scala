/*
 This fails to grab the last few elements of 
 each batch because of a race condition in the DoFn!!!
 */

package trackrecon.dataflow

import com.spotify.scio._
import com.spotify.scio.values.{PairSCollectionFunctions, SideOutput}
import com.spotify.scio.util._
import com.spotify.scio.transforms._
import com.spotify.scio.bigquery._

import java.net.URI
import java.io.{File,PrintWriter}
import java.nio.file.Path
import java.util.Scanner
import java.util.ArrayList

import scala.collection.JavaConversions._

import org.apache.beam.sdk.transforms.windowing.{WindowFn, BoundedWindow}
import org.apache.beam.sdk.io.gcp.bigquery.BigQueryIO
import org.apache.beam.sdk.transforms.DoFn._
import org.apache.beam.sdk.transforms.{DoFn, ParDo}

import com.google.api.services.bigquery.model.TableRow
import com.google.common.collect._

import java.util.UUID.randomUUID

import scala.collection.JavaConverters._
import scala.collection.mutable.ArrayBuffer

import com.github.tototoshi.csv._

import org.slf4j.LoggerFactory


object BigQueryToEventsGCS {

  val SIMPLE_COLUMNS = List(
    "hit_id",
    "x", "y", "z",
    "volume_id", "layer_id", "module_id",
    "tx", "ty", "tz",
    "tpx", "tpy", "tpz",
    "particle_id", "nhits",
    "weight")

  val QUERY = """#standardsql
SELECT events.event_id as event_id, 
     ARRAY_AGG(STRUCT(
         hit_id as hit_id, 
         x as x,
         y as y,
         z as z,
         volume_id as volume_id,
         layer_id as layer_id,
         module_id as module_id,
         cells as cells,
         tx as tx,
         ty as ty,
         tz as tz,
         tpx as tpx,
         tpy as tpy,
         tpz as tpz,
         weight as weight,
         events.particle_id as particle_id,
         particles.nhits as nhits)) as hits
FROM  `train.events` as events
INNER JOIN `train.particles` as particles ON particles.particle_id = events.particle_id AND particles.event_id = events.event_id
GROUP BY event_id
"""

  def formatRecordLn(record: TableRow): String = {

    val eid = record.getLong("event_id")
    val hits_record = record.getRepeated("hits").map(_.asInstanceOf[TableRow])

    hits_record.map { r =>
      val row_strs = for { col <- SIMPLE_COLUMNS } yield {
        Option(r.getString(col)).getOrElse("NaN")
      }

      val cells = r.getRepeated("cells").map(_.asInstanceOf[TableRow])

      val cells_str = "[" ++ cells.flatMap{ cell_trow =>

        val cell_row_str = for {
          ch <- List("ch0", "ch1", "value")
        } yield {
          Option(cell_trow.getString(ch)).getOrElse("NaN")
        }

        ";[" ++ cell_row_str.foldLeft("")(_ ++ ";" ++ _).drop(1) ++ "]"
      }.drop(1) ++ "]"

      eid.toString() ++ "," ++ row_strs.foldLeft("")(_ ++ "," ++ _.toString()).drop(1) ++ "," ++ cells_str ++ "\n"
    }.mkString

  }

  class collectEveryNDoFn[InputT](n: Int)
      extends DoFn[InputT, List[InputT]] {

    var _buffer: ArrayBuffer[InputT] = ArrayBuffer()

    @Setup
    private[dataflow] def setup(): Unit = {
      this._buffer = ArrayBuffer()
    }

    @ProcessElement
    private[dataflow] def processElement(c: DoFn[InputT, List[InputT]]#ProcessContext): Unit = {

      this._buffer.append(c.element())

      if (this._buffer.length == this.n) {
        c.output(this._buffer.toList)
        this._buffer = ArrayBuffer()
      }

    }

  }

  private val logger = LoggerFactory.getLogger(this.getClass)

  def main(cmdlineargs: Array[String]) = {

    val (sc, args) = ContextAndArgs(cmdlineargs)

    val rem = new RemoteFileUtil()

    sc.bigQuerySelect(QUERY)
      .map(formatRecordLn)
      //.applyTransform(ParDo.of(new collectEveryNDoFn[String](9)))
      .map(_.mkString)
      .map{ ccs => {
        val file_name = "batch-" ++ randomUUID().toString
        val bucket_uri = "gs://trackrecon-204713/train/events/"
        val local_file = new File("/tmp/" ++ file_name)
        val w = new PrintWriter(local_file)
        w.write(ccs)
        w.close()
        rem.upload(local_file.toPath(),
          new URI(bucket_uri ++ file_name ++ ".csv"))
      }}

    sc.close()
  }

}
