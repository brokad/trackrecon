package trackrecon.dataflow

import com.spotify.scio._
import com.spotify.scio.values.{PairSCollectionFunctions, SideOutput}
import com.spotify.scio.util._
import com.spotify.scio.transforms._

import java.net.URI
import java.io.File
import java.util.Scanner
import java.util.ArrayList

import org.apache.beam.sdk.transforms.windowing.{WindowFn, BoundedWindow}
import org.apache.beam.sdk.io.gcp.bigquery.BigQueryIO

import com.google.api.services.bigquery.model.TableRow

import scala.collection.JavaConverters._

import com.github.tototoshi.csv._

import org.slf4j.LoggerFactory


object EventsToBigQuery {

  private val logger = LoggerFactory.getLogger(this.getClass)

  // BigQuery spits any non-JSON compliant entry out; so we filter NaNs and Infs
  def safe_set(t: TableRow, col: (String, String)) = {
    try {
      val entry = col._2.toDouble
      if (entry.isNaN || entry.isInfinity) {
        logger.warn("A NaN or INF entry was dropped from " + t.get("event_id") + " " + t.get("hit_id"))
        t
      } else t.set(col._1, col._2)
    } catch {
      case _ : Throwable => t
    }
  }

  def updateTableRow(table_row: TableRow, t: String, labeled_rows: List[List[(String, String)]]) = {
    if (t != "cells") labeled_rows.head.foldLeft(table_row)(safe_set)
    else {
      val cells_record = new ArrayList[TableRow]()
      for (row <- labeled_rows) {
        cells_record.add(row.foldLeft(new TableRow())(safe_set))
      }
      table_row.set("cells", cells_record)
    }
  }

  def main(cmdlineargs: Array[String]) = {

    val (sc, args) = ContextAndArgs(cmdlineargs)

    val rem = new RemoteFileUtil()

    val hits_columns = List("x", "y", "z", "volume_id", "layer_id", "module_id")
    val cells_columns = List("ch0", "ch1", "value")
    val truth_columns = List("particle_id", "tx", "ty", "tz", "tpx", "tpy", "tpz", "weight")

    sc.parallelize(1000 to 10000)
      .flatMap(e => {
        val base_path = "gs://trackrecon-204713/vanilla/event00000" + e + "-"
        val event_uris = for (
          p <- List("hits", "truth", "cells")
        ) yield (p, new URI(base_path + p + ".csv"))

        if (event_uris.forall { case (_, uri) => rem.remoteExists(uri) }) {
          List((e, for ((t, uri) <- event_uris) yield uri))
        } else List.empty
      })
      .map { case (e, uris) => {
        (e, uris.map(uri => {
          val path = rem.download(uri)
          val reader = CSVReader.open(path.toFile())
          val data = reader.all()
          reader.close()
          data.drop(1)}))
      }}
      .map { case (e, ees) => {
        (e, List("hits", "truth", "cells") zip ees.map(_.groupBy(_.head)))
      }}
      .map { case (e, eesms) =>
        (e, Map(eesms.map({ case (t, eesm) => {
          (t, eesm.mapValues { _.map { (t match {
          case "hits" => hits_columns
          case "cells" => cells_columns
          case "truth" => truth_columns
          }) zip _.drop(1) }})}}): _*))
      }
      .flatMap { case (e, eesms) => {
        for ((hit_id, hit_row) <- eesms("hits")) yield {
          val trow = new TableRow()
          updateTableRow(trow, "hits", hit_row)

          eesms("truth").get(hit_id) match {
            case Some(t) => updateTableRow(trow, "truth", t)
            case None => trow
          }

          eesms("cells").get(hit_id) match {
            case Some(c) => updateTableRow(trow, "cells", c)
            case None => trow
          }

          trow.set("hit_id", hit_id)
          trow.set("event_id", e.toString)
        }
      }}
      .saveAsBigQuery("train.events",
        createDisposition=BigQueryIO.Write.CreateDisposition.CREATE_NEVER,
        writeDisposition=BigQueryIO.Write.WriteDisposition.WRITE_TRUNCATE)

    sc.close()
  }

}
