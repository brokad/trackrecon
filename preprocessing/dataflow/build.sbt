
libraryDependencies ++= Seq(
  "com.spotify" %% "scio-core" % "0.5.2",
  "com.spotify" %% "scio-test" % "0.5.2" % "test",
  "org.apache.beam" % "beam-runners-direct-java" % "2.4.0",
  "org.apache.beam" % "beam-runners-google-cloud-dataflow-java" % "2.4.0",
  "org.slf4j" % "slf4j-simple" % "1.6.4"
)

libraryDependencies += "com.github.tototoshi" %% "scala-csv" % "1.3.5"

addCompilerPlugin("org.scalamacros" % "paradise" % "2.1.1" cross CrossVersion.full)
