FROM nixos/nix:latest

ADD trackrecon /opt/trackrecon/
ADD trackrecon-4ba9d4e5fd7b.json /opt/trackrecon-4ba9d4e5fd7b.json
ADD nixpkgs.nix /opt/nixpkgs.nix
ADD trackrecon.nix /opt/trackrecon.nix
ADD shell.nix /opt/shell.nix

WORKDIR /opt/
RUN nix-shell shell.nix
