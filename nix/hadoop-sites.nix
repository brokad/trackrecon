{config, pkgs, ... }:
{

  services.hadoop.coreSite = {
    "fs.default.name" = "hdfs://trackrecon-virt-m:9000";
  };

  services.hadoop.hdfsSite = {
    "dfs.replication" = "1";
    "dfs.permissions" = "false";  # uh oh
  };

  services.hadoop.mapredSite = {
    "mapreduce.framework.name" = "yarn";

    "mapreduce.map.memory.mb" = "2048";
    "mapreduce.reduce.memory.mb" = "4096";

    "mapreduce.map.java.opts" = "-Xmx2048m";
    "mapreduce.reduce.java.opts" = "-Xmx4096m";
  };
  
  services.hadoop.yarnSite = {
    "yarn.acl.enable" = "true";
    "yarn.resourcemanager.hostname" = "trackrecon-virt-m";

    "yarn.nodemanager.aux-services" = "mapreduce_shuffle";
    "yarn.nodemanager.resource.memory-mb" = "14336";
    "yarn.nodemanager.resource.cpu-vcores" = "4";
    "yarn.nodemanager.vmem-check-enabled" = "false";

    "yarn.scheduler.capacity.root.queues" = "default";
    "yarn.scheduler.capacity.root.default.capacity" = "100";
    "yarn.scheduler.capacity.maximum-am-resource-percent" = "0.8";

    "yarn.scheduler.minimum-allocation-mb" = "2048";
    "yarn.scheduler.maximum-allocation-mb" = "6144";
    
    "yarn.nodemanager.log-dirs" = "/tmp/userlogs";
  };
  
}
