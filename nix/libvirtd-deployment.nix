{
  deployment = {
    network.description = "train locally in a vm (for testing purposes)";
  };

  machine = node: { config, pkgs, ... }: {
    deployment.targetEnv = "libvirtd";
    deployment.libvirtd.memorySize = 16384;
    deployment.libvirtd.vcpu = 4;
  };
}
