let

target = import ./gce-deployment.nix;
cluster = import ./trackrecon-virt.nix;

in cluster { inherit target; }
