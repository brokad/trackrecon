{ nixpkgs,
  buildPythonPackage ? nixpkgs.python3Packages.buildPythonPackage,
  fetchPypi ? nixpkgs.python3Packages.fetchPypi }:

let

  google_resumable_media = buildPythonPackage rec {

    pname = "google-resumable-media";
    version = "0.3.1";

    propagatedBuildInputs = with nixpkgs.python3Packages; [
      six
    ];

    src = fetchPypi {
       inherit pname version;
       sha256 = "0dlqwif4zm6yr1z6c7lqf2wxn6fkph4b7yk11g645m36h67m3plp";
    };

    doCheck = false;
  
  };

  google_cloud_storage = buildPythonPackage rec {

    pname = "google-cloud-storage";
    version = "1.10.0";

    propagatedBuildInputs = with nixpkgs.python3Packages; [
      google_cloud_core
      google_api_core
      google_auth
      google_resumable_media
    ];
  
    src = fetchPypi {
       inherit pname version;
       sha256 = "0biycsd787qdcik8zcxckz0pzmqrqq0hrxl99z7r8ycdvxc9b5n1";
    };

    doCheck = false;
  
  };

in buildPythonPackage rec {
  
  pname = "trackrecon";
  version = "0.1.dev0";
  
  src = ../trackrecon;
  
  SOURCE_DATE_EPOCH=315532800;
  
  propagatedBuildInputs = with nixpkgs.python3Packages;
  [
    pandas
    numpy
    sortedcontainers
    grpcio
    tensorflow
    scikitlearn
    cloudpickle
    google_cloud_storage
  ];
  
  doCheck = false;
  
}
