let

target = import ./libvirtd-deployment.nix;
cluster = import ./trackrecon-virt.nix;

in cluster { inherit target; }
