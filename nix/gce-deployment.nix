with <nixpkgs/lib>;

let

credentials = {
  project="trackrecon-204713";
  serviceAccount = "devops-trainer@trackrecon-204713.iam.gserviceaccount.com";
  accessKey = "./trackrecon-204713-b862e85839ba.pkey.pem";
};

gce = node: { ... }: {
  deployment.targetEnv = "gce";
  deployment.gce = credentials // {
    region = "us-central1-b";
    tags = [ "trainer" ];
    rootDiskSize = 64;
    instanceType = "n1-standard-4";
    guestAccelerators = lib.mkIf (node == "worker") {
      acceleratorCount = 1;
      acceleratorType = "nvidia-tesla-v100";
    };
    scheduling.onHostMaintenance = "TERMINATE";
  };
};

in {

  deployment = {
    network.description = "train on gce";
  };

  machine = gce;

}

  
  
