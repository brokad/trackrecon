{ target }:

let

  numNodes = 1;
  numExecutors = 4 * numNodes;
  executorCores = 1;
  
  trainer = { config, pkgs, ... }:
  let

    hadoopConf = import <nixpkgs/nixos/modules/services/cluster/hadoop/conf.nix>
      { hadoop = config.services.hadoop; inherit pkgs; };

    clusterConf =
    { inherit hadoopConf;
      inherit numExecutors;
      inherit executorCores; };

  in import ./trainer.nix { nixpkgs = pkgs; inherit clusterConf; };

  GCP_CREDENTIALS = builtins.readFile ../trackrecon-4ba9d4e5fd7b.json;

  makeWorkerNode = n: {
    name = "trackrecon-virt-w-" + builtins.toString n;
    value = { config, pkgs, ... } @ args:
    {
      networking.firewall.enable = false;  # uh oh
    
      imports = [ ./hadoop-sites.nix ];

      environment.systemPackages =
      let
        trackrecon = import ./trackrecon.nix { nixpkgs = pkgs; };
        python-w-trackrecon = pkgs.python3.withPackages (
          pp: [ trackrecon ]
        );
      in [ python-w-trackrecon ];

      environment.etc."gcp-credentials.json".text = GCP_CREDENTIALS;
    
      services.hadoop.hdfs.datanode = {
        enabled = true;
      };

      services.hadoop.yarn.nodemanager = {
        enabled = true;
      };
    } // (target.machine "worker" args);
  };

  workers = with builtins; listToAttrs (genList makeWorkerNode numNodes);
    
in {
  
  trackrecon-virt-m = { config, pkgs, ... } @ args:
  {
    environment.systemPackages = [ (trainer args) ];

    networking.firewall.enable = false;  # uh oh
    
    imports = [ ./hadoop-sites.nix ];
    
    services.hadoop.hdfs.namenode = {
      enabled = true;
    };

    services.hadoop.yarn.resourcemanager = {
      enabled = true;
    };
    
  } // (target.machine "master" args);
  
} // workers // target.deployment
