{ nixpkgs ? import ./nixpkgs.nix {},
  stdenv ? nixpkgs.stdenv,
  trackrecon ? import ./trackrecon.nix { inherit nixpkgs; },
  clusterConf }:

let

  python-w-trackrecon = nixpkgs.python3.withPackages (pp:
  [
    trackrecon
  ]);

  spark = nixpkgs.spark.override {
    pythonPackages = python-w-trackrecon.pkgs;
  };

  hadoopConf = clusterConf.hadoopConf;
  numExecutors = clusterConf.numExecutors;
  executorCores = clusterConf.executorCores;

in
  
  stdenv.mkDerivation rec {
  
    version = trackrecon.version;
    name = trackrecon.pname + "-" + version + "-trainer";

    buildInputs = [
      spark trackrecon
    ];

    inherit trackrecon;
    inherit spark;

    inherit hadoopConf;
    inherit numExecutors;
    inherit executorCores;

    GOOGLE_APPLICATION_CREDENTIALS = ../trackrecon-4ba9d4e5fd7b.json;

    task = trackrecon + /lib/python3.6/site-packages/trainer/task.py;

    builder = builtins.toFile "builder.sh" ''
      source $stdenv/setup

      mkdir -p $out/bin

      cat > $out/bin/train <<- EOF
      export HADOOP_CONF_DIR=$hadoopConf
      export PYTHONPATH="\$PYTHONPATH:$PYTHONPATH"
      export GOOGLE_APPLICATION_CREDENTIALS=$GOOGLE_APPLICATION_CREDENTIALS
      exec $spark/bin/spark-submit --master yarn \
                                   --conf spark.executorEnv.GOOGLE_APPLICATION_CREDENTIALS=/etc/gcp-credentials.json \
                                   --num-executors=$numExecutors \
                                   --executor-cores=$executorCores \
                                   $task "\$@"
      EOF
      chmod +x $out/bin/train

      cat > $out/bin/train-repl <<- EOF
      export HADOOP_CONF_DIR=$hadoopConf
      export PYTHONPATH="\$PYTHONPATH:$PYTHONPATH"
      export GOOGLE_APPLICATION_CREDENTIALS=$GOOGLE_APPLICATION_CREDENTIALS
      exec $spark/bin/pyspark --master yarn \
                              --conf spark.executorEnv.GOOGLE_APPLICATION_CREDENTIALS=/etc/gcp-credentials.json \
                              --num-executors=$numExecutors \
                              --executor-cores=$executorCores \
                              "\$@"
      EOF
      chmod +x $out/bin/train-repl
    '';
  
  }
