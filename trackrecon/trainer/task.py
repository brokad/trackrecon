import sys
import logging
import argparse
import hashlib
import time

from pgot.trees import HoughTreeEnsemble
from pgot.houghspace import LinesModuliSpace
from pgot.strategy import LargestClusterWins
from pgot.learner import DBSCAN
from pgot.scoring import delta_score_fn
from pgot.binning import BinnedEvent

from trainer.worker import trainAndScoreDoFn, filterDoFn
from trainer.factories import DataFactory

import pandas as pd
import numpy as np

from pyspark import SparkContext


BOUNDING_BIN = [[0, 2*np.pi],
                [0, 6]]

HOUGH_MAP = LinesModuliSpace

DEFAULT_TREE = HoughTreeEnsemble(learner=DBSCAN(BOUNDING_BIN),
                                 strategy=LargestClusterWins(BOUNDING_BIN),
                                 box=BOUNDING_BIN,
                                 hough_map=HOUGH_MAP)


def main():

    parser = argparse.ArgumentParser()

    parser.add_argument("--enable-testing", action="store_true",
                        help="enables testing environment "
                        "(max verbosity, only samples a few events"
                        " for training, etc)")

    parser.add_argument("--n-keep-events", default="16",
                        help="the number of events to keep in testing."
                        "Only applies when --enable-testing is True",
                        type=int)

    parser.add_argument("--truncate-events", action="store_true",
                        help="wether each event should be truncated "
                        "(used for testing)")

    parser.add_argument("--filter-engine", default="python",
                        help="",
                        type=str)

    parser.add_argument("--staging", default="gcs://trackrecon-204713",
                        help="a staging area (bucket or local)",
                        type=str)

    parser.add_argument("--job",
                        help="an id of an existing job (for resuming)",
                        type=str)

    parser.add_argument("--tmp", default="/tmp",
                        help="local directory for storing"
                        " temporary training files",
                        type=str)

    parser.add_argument("--target-tree-depth", default=300,
                        help="how deep to grow the tree in this job",
                        type=int)

    parser.add_argument("--min-num-stages", default=5,
                        help="the minimum number of stages"
                        " to grow per leaf in training",
                        type=int)

    parser.add_argument("--pool", default="gcs://trackrecon-204713",
                        help="the location (could be a bucket"
                        ", could be local) of the data pool to"
                        "use for training",
                        type=str)

    parser.add_argument("--dataset", default="train/events/",
                        help="the location of source blobs used"
                        " for training (relative to pool)",
                        type=str)

    args = parser.parse_args()

    dataset = args.dataset

    distr_format = "spark.rdd"

    if not args.job:

        now = str(time.time())
        job = "trackrecon-job-" + hashlib.sha1(now.encode())\
                                         .hexdigest()

    else:

        job = args.job

    pool = args.pool

    tmp = args.tmp

    staging = args.staging

    MIN_NUM_STAGES = args.min_num_stages
    STARTING_TREE_DEPTH = 0
    TARGET_TREE_DEPTH = args.target_tree_depth
    TREE = DEFAULT_TREE

    OBJECTIVE_FN = delta_score_fn

    logger = logging.getLogger("pgot")
    handler = logging.StreamHandler()
    fmt = '%(asctime)s (pgot) %(unitname)s - %(levelname)s: %(message)s'
    formatter = logging.Formatter(fmt=fmt)
    handler.setFormatter(formatter)
    logger.addHandler(handler)

    def log(level, msg, *args):
        logfn = getattr(logger, level)
        logfn(msg, *args, extra={'unitname': "trainer"})

    log("debug", "loading data: %s", {dataset: dataset,
                                      distr_format: distr_format})

    f = DataFactory(job, tmp, staging,
                    pool=pool, dataset=dataset,
                    distr_format=distr_format,
                    testing=args.enable_testing,
                    n_keep_events=args.n_keep_events,
                    truncate_events=args.truncate_events)

    if args.enable_testing:
        logger.setLevel(logging.DEBUG)
        log("critical", "Training in testing mode!! "
            "Data is truncated arbitrarily for performance.")

    else:
        logger.setLevel(logging.WARNING)

    sc = SparkContext(appName="particles grow on trees")

    erdd = f.toSpark(sc)

    def preprocess(df):
        df["arcxy"] = np.arctan2(df["x"], df["y"])
        return [BinnedEvent("", df)]

    erdd = erdd.map(preprocess)

    log("info", "training")

    for i in range(STARTING_TREE_DEPTH, TARGET_TREE_DEPTH):

        log("debug", "at tree step %i...", i)

        log("debug", "...breeding candidates at the leaves")

        for leaf in TREE.leaves():
            while len(leaf.staging) < MIN_NUM_STAGES:
                leaf.stage(leaf.withSplitAt(leaf.box.randomCut()))

        erdd = erdd.map(filterDoFn(TREE),
                        preservesPartitioning=True)

        erdd.cache()  # will be used to rebin next time around

        escores = erdd.flatMap(trainAndScoreDoFn(TREE, OBJECTIVE_FN))
        sscores = escores.reduceByKey(pd.Series.__add__)

        log("debug", "...training, scoring and finding the best split")

        best = sscores.max(key=lambda ss: ss[1].max())

        log("debug", "...best split is at leaf %s, stage number %i, and "
            "it lead to a delta of %f",
            best[0], best[1].idxmax(), best[1].max())

        TREE.getNode(best[0]).commit(best[1].idxmax())

        log("debug", "...saving a snapshot of the (trained) tree to gcs")

        f.saveToStaging(TREE)


if __name__ == "__main__":
    print(">>> this is `particles grow on trees`,\n"
          ">>> by @brokad (damienbroka@mailbox.org)\n"
          ">>> made for the 2018 CERN TrackML Kaggle competition")
    print(sys.argv)
    main()
