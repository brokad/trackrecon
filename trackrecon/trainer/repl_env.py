import pandas as pd
import numpy as np

from pgot.trees import HoughTreeEnsemble
from pgot.parameter_space import LinesModuliSpace
from pgot.strategy import LargestClusterWins
from pgot.learner import DBSCAN

from trainer.factories import DataFactory

from pgot.hough import HoughHistogramImmutableFunctions


def _delta_score_fn(orig, stages):

    orig_t = orig["score"].sum()
    stages_d = pd.Series([stage["score"].sum() - orig_t
                          for stage in stages])
    return stages_d


BOUNDING_BIN = [[0, 6],
                [0, 2*np.pi]]

HOUGH_MAP = LinesModuliSpace()

DEFAULT_TREE = HoughTreeEnsemble(learner=DBSCAN(BOUNDING_BIN),
                                 strategy=LargestClusterWins(BOUNDING_BIN),
                                 box=BOUNDING_BIN,
                                 hough_map=HOUGH_MAP)

tree = DEFAULT_TREE

hfn = HoughHistogramImmutableFunctions(tree)

f = DataFactory("repl", "/tmp", "/tmp",
                "gcs://trackrecon-204713",
                "train/events/", "spark.rdd",
                testing=True, n_keep_events=1, truncate_events=False)

df = next(f.cache_it(0))

df["arcxy"] = np.arctan2(df["x"], df["y"])
