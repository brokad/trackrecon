import pandas as pd

from pgot.hough import HoughHistogramImmutableFunctions


def withObjectiveFn(objFn):

    def makeWrapped(fn):

        def wrappedFn(*args, **kwargs):

            orig_, stages_ = fn(*args, **kwargs)
            obj = objFn(orig_, stages_)
            return obj

        return wrappedFn

    return makeWrapped


def mapFork(fn):

    def wrappedFn(bevent):

        for leaf_id, hits in bevent:

            yield (leaf_id, fn(leaf_id, hits))

    return wrappedFn


def trainAndScoreDoFn(of_tree, with_obj_fn):

    @mapFork
    @withObjectiveFn(with_obj_fn)
    def scoreLeafFn(leaf_id, event):

        leaf = of_tree.getNode(leaf_id)

        orig_c = pd.concat([event, leaf.fit_predict(event)], axis=1)

        orig_ = leaf.track_scores(orig_c)

        stages_ = []

        for staged in leaf.staging:

            staged_c = pd.concat([event, staged.fit_predict(event)], axis=1)

            stage_ = staged.track_scores(staged_c)

            stages_ += [stage_]

        return orig_, stages_

    return scoreLeafFn


def filterDoFn(of_tree):

    hfn = HoughHistogramImmutableFunctions(of_tree)

    def filterFn(bevent):
        o = list(hfn.withBins(bevent))
        return o

    return filterFn
