from google.cloud import storage as gcs

import pandas as pd
import numpy as np

import logging
import io

import cloudpickle

import os

import ast


NUM_TRAIN_EVENTS = 9047
NUM_TRAIN_TRACKS = 82255148


nested_array = lambda c: ast.literal_eval(c.replace(";", ","))


# for train/events/
COLUMN_TYPES = [('event_id', int),
                ('hit_id', int),
                ('x', float), ('y', float), ('z', float),
                ('volume_id', int), ('layer_id', int), ('module_id', int),
                ('tx', float), ('ty', float), ('tz', float),
                ('tpx', float), ('tpy', float), ('tpz', float),
                ('particle_id', int),
                ('nhits', int),
                ('weight', float),
                ('cells', nested_array)]


'''
for train/tracks/
COLUMN_TYPES = [('unique_id', str),
                ('hit_id', np.int32),
                ('x', np.float64), ('y', np.float64), ('z', np.float64),
                ('volume_id', np.int32), ('layer_id', np.int32), ('module_id', np.int32),
                ('tx', np.float64), ('ty', np.float64), ('tz', np.float64),
                ('tpx', np.float64), ('tpy', np.float64), ('tpz', np.float64),
                ('weight', np.float64),
                ('nhits', np.int32),
                ('cells', nested_array)]
'''


COLUMN_NAMES = list(map(lambda x: x[0], COLUMN_TYPES))


class DataFactory:

    def __init__(self, job, tmp, staging,
                 pool, dataset,
                 distr_format='local', testing=False,
                 n_keep_events=1,
                 truncate_events=False):
        '''Utility object to gather the dataset sampled
        and stored on the project's bucket.
        Provides caching of data needed on a per sample basis for local use.

        Parameters
        ----------
        source : str
           the prefix location of batched dataset
           to consider (eg. 'train/tracks/')

        distr_format : str {'local', 'spark.rdd', 'spark.dataframe'}
           describes the desired high-level format of loaded dataset
        '''

        if distr_format not in ["spark.dataframe",
                                "spark.rdd",
                                "local"]:

            raise ValueError("Invalid value for argument 'distr_format'.")

        self.job = job
        self.tmp = tmp

        self.staging = staging

        self._source = dataset

        self.testing = testing
        self.n_keep_events = n_keep_events
        self.truncate_events = truncate_events

        self.snapshot_counter = -1

        if DataFactory.isGCSLocation(pool):

            self.bucket = DataFactory.stripGCSPrefix(pool)

            client = gcs.Client()
            bucket_ = client.get_bucket(self.bucket)

            self._blobs = [blob.name
                           for blob in bucket_.list_blobs(prefix=self._source,
                                                          delimiter="/")]

            if self.testing:
                self._blobs = self._blobs[:self.n_keep_events]

            if any(not blob for blob in self._blobs):
                raise FileNotFoundError(
                    "Given pool bucket is invalid or gcs is unreachable."
                )

        else:

            raise NotImplementedError("Can't load data from "
                                      "local source for now.")

        self._cache = {}
        self._distr_format = distr_format

    @classmethod
    def isGCSLocation(cls, loc):
        return "gcs://" == loc[:6]

    @classmethod
    def stripGCSPrefix(cls, loc):
        return loc[6:]

    def toSpark(self, sc):

        index_rdd = sc.parallelize(range(0, len(self._blobs)))

        cache = index_rdd.flatMap(self.cache_it, preservesPartitioning=True)

        if self._distr_format == "spark.dataframe":

            from pyspark.sql import SQLContext

            sqlc = SQLContext(sc)
            cache_df = sqlc.createDataFrame(cache)

            return cache_df

        else:

            return cache

    def cache_it(self, blob_index):

        from google.cloud import storage

        client_ = storage.Client()
        bucket_ = client_.get_bucket(self.bucket)

        temp_filename = "/tmp/trackrecon-temp-blob-" + str(blob_index) + ".csv"

        if not os.path.isfile(temp_filename):

            with open(temp_filename, "wb") as f:

                bucket_.get_blob(self._blobs[blob_index]).download_to_file(f)

        from pyspark.sql import Row

        def sparkify(x):

            row = {COLUMN_TYPES[i][0]: v
                   for i, v in enumerate(x)}

            if self._distr_format == "spark.dataframe":

                return Row(row)

            else:

                return row

        with open(temp_filename, "r") as f:

            if self.truncate_events:

                source = f.readlines(2 ** 12)

            else:

                source = f

            raw_hits = [sparkify([COLUMN_TYPES[i][1](c)
                                  for i, c in enumerate(ln.split(","))])
                        for ln in source]

        if self._distr_format == "spark.dataframe":

            yield from raw_hits

        else:

            yield pd.DataFrame(raw_hits)

    def saveToStaging(self, obj):

        stage_is_remote = DataFactory.isGCSLocation(self.staging)

        pobj = cloudpickle.dumps(obj)
        self.snapshot_counter += 1

        blob_name = self.job + "-snapshot-" + str(self.snapshot_counter)
        job_dir = os.path.join(self.staging, self.job)

        pobj_file_dir = self.tmp if stage_is_remote else job_dir
        local_snap_path = os.path.join(pobj_file_dir, blob_name)

        with open(local_snap_path, "wb") as f:
            f.write(pobj)

        if stage_is_remote:

            gcs_snap_path = os.path.join(self.job, blob_name)

            client = gcs.Client()
            staging_bucket_name = DataFactory.stripGCSPrefix(self.staging)
            staging_bucket = client.get_bucket(staging_bucket_name)

            job_blob = staging_bucket.blob(gcs_snap_path)

            with open(local_snap_path, "rb") as f:
                job_blob.upload_from_file(f)
