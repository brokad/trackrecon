import pandas as pd
import numpy as np

from functools import reduce

from pgot.propagates import Propagates


class Strategy(Propagates):

    def __init__(self, box, integrity_checks=True):
        super().__init__(box, integrity_checks=integrity_checks)

    def fit_ensemble(self, *args):

        # td: integrity_checks; but for now we don't fit anyway
        return self.do_fit_ensemble(*args)

    def predict_ensemble(self, *args):

        self.log("debug", "ensembling %i bags", len(args))

        if self.integrity_checks:

            self.check(*args)

        ensemble = self.do_predict_ensemble(*args)

        return ensemble

    def check(self, *args):

        self.log("debug", "doing integrity checks")

        conflict_idx = reduce(lambda x, y: x.intersection(y),
                              map(lambda bag: bag.index, args))

        nconflicting = len(conflict_idx)

        self.log("debug", "predictions are conflicted on %i indices",
                 nconflicting)

        ensemble_cluster_ids = set()

        for i, bag in enumerate(args):

            dup_idx = bag.index.duplicated(keep='first')

            if np.any(dup_idx):

                self.log("critical", "bag %i has duplicated indices", i)

                raise ValueError("Predictions have duplicated indices.")

            bag_cluster_ids = set(bag["cluster_id"][bag["cluster_id"] != 0])

            if ensemble_cluster_ids.intersection(bag_cluster_ids):

                self.log("critical",
                         "bag %i has cluster_ids clashing with"
                         " other bags in the ensemble", i)

                raise ValueError("Predictions in ensemble have"
                                 " clashing cluster_ids")

            else:

                ensemble_cluster_ids.update(bag_cluster_ids)

    def do_fit_ensemble(self, *args):

        raise NotImplementedError("You need to define a "
                                  "do_fit_ensemble method.")

    def do_predict_ensemble(self, *args):

        raise NotImplementedError("You need to define a "
                                  "do_predict_ensemble method.")


class LargestClusterWins(Strategy):

    def do_fit_ensemble(self, left, right):
        pass

    def do_predict_ensemble(self, left, right):
        '''
        Note this does not check for clashes in cluster_ids.
        This also assumes left and right share index sets at the level of events.
        This will throw a ValueError if left and/or right have duplicates in 
        order to prevent them from propagating
        '''

        ensembled = []

        left_, right_ = [side[side["cluster_id"] != 0]
                         for side in (left, right)]

        l_noise, r_noise = [side[side["cluster_id"] == 0]
                            for side in (left, right)]

        lc, rc = [{cluster_id: cluster_hits
                  for cluster_id, cluster_hits in side.groupby("cluster_id")}
                  for side in (left_, right_)]

        lns, rns = [pd.Series({k: len(v) for k, v in ccs.items()})
                    for ccs in (lc, rc)]

        def winner_loser(winning_side):

            refs = [[left_, lc, lns, l_noise],
                    [right_, rc, rns, r_noise]]

            if winning_side == "left":

                return refs

            else:

                return reversed(refs)

        while lc and rc:

            winning_side = "left" if lns.max() >= rns.max() else "right"

            [[wp, wpcs, wpns, wpnoise],
             [lp, lpcs, lpns, lpnoise]] = winner_loser(winning_side)

            cluster_id = wpns.idxmax()

            for hit_idx in wpcs[cluster_id].index:

                try:

                    c_id = lp.loc[hit_idx]["cluster_id"]

                except KeyError:

                    continue

                lpcs[c_id].drop(hit_idx, axis=0,
                                inplace=True)
                lpns[c_id] -= 1

                if lpcs[c_id].empty:
                    del lpcs[c_id]
                    assert lpns[c_id] == 0  # this shouldn't happen

            ensembled += [wpcs[cluster_id]]
            del wpcs[cluster_id]
            wpns[cluster_id] = 0

        ensembled += list(lc.values()) + list(rc.values())

        noise_idx = l_noise.index.union(r_noise.index)
        noise = pd.DataFrame(np.zeros(len(noise_idx)),
                             columns=['cluster_id'],
                             index=noise_idx)

        if ensembled:

            ensemble_df = pd.concat(ensembled, axis=0)

        else:

            return noise

        for hit_idx in ensemble_df.index:

            try:

                noise.drop(hit_idx, axis=0,
                           inplace=True)

            except KeyError:

                continue

        ens_w_noise = pd.concat([ensemble_df, noise], axis=0,
                                verify_integrity=True)
        return ens_w_noise
