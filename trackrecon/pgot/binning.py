import numpy as np
import pandas as pd

from copy import deepcopy


class Constraint:

    def __init__(self, axis, value):
        self.axis = axis
        self.val = value

    def __leq__(self, other):
        return self._val <= other[self._axis]

    def __geq__(self, other):
        return self._val >= other[self._axis]

    def __lt__(self, other):
        return self._val < other[self._axis]

    def __gt__(self, other):
        return self._val > other[self._axis]


class Bin:

    def __init__(self, bounds):

        if isinstance(bounds, Bin):

            self.bounds = bounds.bounds

        else:

            self.bounds = bounds

    def __iter__(self):

        yield from self.bounds.__iter__()

    def splitAt(self, constraint):
        # !! No test is being made to make sure constraint is compatible !!
        left_bounds = deepcopy(self.bounds)
        right_bounds = deepcopy(self.bounds)

        left_bounds[constraint.axis][1] = constraint.val
        right_bounds[constraint.axis][0] = constraint.val

        return (Bin(left_bounds), Bin(right_bounds))

    def randomCut(self, margin=0.0):

        axis = np.random.randint(0, len(self.bounds))
        low, high = self.bounds[axis]

        m_low = margin * (high - low) + low
        m_high = high - margin * (high - low)
        value = np.random.random()*(m_high - m_low) + m_low

        return Constraint(axis, value)


class BinnedEvent:

    def __init__(self, leaf_id, event):

        self.leaf_id = leaf_id
        self.event = event

    def __iter__(self):

        yield from (self.leaf_id, self.event)
