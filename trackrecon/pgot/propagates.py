import logging

from pgot.binning import Bin


class Propagates:

    def __init__(self, box, **kwargs):

        self.box = Bin(box)

        for k, v in kwargs.items():
            setattr(self, k, v)

        self._opts = list(kwargs.keys())

    def log(self, level, msg, *args):

        me = self.__class__.__name__
        logger = logging.getLogger("pgot.{0}".format(me))
        logfn = getattr(logger, level)
        logfn(msg, *args, extra={'unitname': me})

    def propagate(self, box, **kwargs):

        opts = {}
        for k in self._opts:

            try:

                opts[k] = self.k

            except AttributeError:

                continue

        # default is trivial propagation
        return self.__class__(box, **kwargs, **opts)

    def __call__(self, box):

        return self.propagate(box)
