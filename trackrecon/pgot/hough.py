import numpy as np
from sortedcontainers import SortedSet

from pgot.trees import LeafMap


class Hit:

    def __init__(self, hit):
        self.hit_id = hit.hit_id

    def __eq__(self, other):
        try:
            return self.hit_id == other.hit_id
        except AttributeError:
            return self.hit_id == other

    def key(self):
        return self.hit_id

    def __hash__(self):
        return self.hit_id.__hash__()


class SortedHitSet(SortedSet):

    def __init__(self, iterable=None):
        super().__init__(iterable, key=Hit.key)

    def get(self, hit):
        try:
            ni = self.index(hit)
        except TypeError:
            ni = self.index(Hit(hit))
        return self[ni]


class HoughBinNode(Hit):

    def __init__(self, hit_id, prev_bin=None, next_bin=None):
        self.prev_bin = prev_bin
        self.next_bin = next_bin
        self.hit_id = hit_id


class HoughBinNodeRecord(Hit):

    def __init__(self, node, a_bin):
        self._node = node
        self.hit_id = self._node.hit_id
        self._bin = a_bin


class HoughHistogramMutable:

    def __init__(self, bin_edges, hough_curve_dofn):
        """A class for handling Hough histograms with arbitrary regular sampling bins and arbitrary family of Hough curves. Handles fast insertions and removals with a view towards use for the clustering of points into instances of the given family.
        """

        self._bin_edges = bin_edges
        self._hough_curve_dofn = hough_curve_dofn

        lens = list(map(len, bin_edges))
        self._m = np.empty(shape=lens, dtype=object)

        for xyz in product(*map(lambda l: range(0, l), lens)):
            self._m[xyz] = SortedHitSet()

        self._hits = SortedHitSet()

    def insert(self, hit, t_from=0, t_to=4*np.pi, num_samples=50):

        # If hit is already in histogram, we first remove it
        if Hit(hit) in self._hits:
            self.remove(Hit(hit))

        #(damien): Brutal sampling at regular intervals is far from ideal. This could be replaced with the partition tree approach of immutable histograms or adaptive sampling using the tangent to the curve at the current sample point (and the dimensions of the current box this falls in).
        hc = np.array(list(map(lambda s: hough_curve(hit, s),
                               np.linspace(t_from, t_to, num_samples))))

        # Points that sampled outside the binning bounding box are dropped
        lower_left = np.array(list(map(min, self._bin_edges)))
        top_right = np.array(list(map(max, self._bin_edges)))

        hc = hc[np.all(hc > lower_left,
                       axis=1)*np.all(hc < top_right,
                                      axis=1), :]

        hit_bins = SortedSet(zip(*[np.searchsorted(edges, hc[:, dim])
                             for dim, edges in enumerate(self._bin_edges)]))

        for i, a_bin in enumerate(hit_bins):
            node = HoughBinNode(hit.hit_id,
                                hit_bins[i-1] if i > 0 else None,
                                hit_bins[i+1] if i + 1 < len(hit_bins)
                                else None)

            if i == len(hit_bins) - 1:
                self._hits.add(HoughBinNodeRecord(node, a_bin))

            self._m[a_bin].add(node)

    def ndmap(self, f):
        out = np.zeros(self._m.shape)
        for xyz, e in np.ndenumerate(self._m):
            out[xyz] = f(e)
        return out

    def remove(self, hit):

        record = self._hits.get(hit)
        self.declean(record._node, record._bin)
        self._hits.remove(record)

    def declean(self, hit, from_pos):

        node = self[from_pos].get(hit)

        next_bin = node.next_bin
        prev_bin = node.prev_bin

        self[from_pos].remove(hit)

        while next_bin is not None:
            entry = self._m[next_bin]
            node = entry.get(hit)
            next_bin = node.next_bin
            entry.remove(node)

        while prev_bin is not None:
            entry = self._m[prev_bin]
            node = entry.get(hit)
            prev_bin = node.prev_bin
            entry.remove(node)

    def __getitem__(self, *args):
        return self._m.__getitem__(*args)


class HoughHistogramImmutableFunctions:

    def __init__(self, tree):
        """A class for binning Hough histograms
        with *arbitrary* bins and Hough curve family.
        This is just a convenience wrapper for the
        parameter space PartitionTree class.
        """
        self.tree = tree

    def withBins(self, bevent):

        for leaf_id, hits in bevent:

            fromTree = self.tree.getNode(leaf_id)

            for rel_id, rel_hits in fromTree.map(LeafMap.Identity, hits,
                                                 as_enumerate=True):

                yield (leaf_id + rel_id, rel_hits)
