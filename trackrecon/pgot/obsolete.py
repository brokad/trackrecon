class HelixModuliSpace:

    def __init__(self, through=set()):
        self.through = through

    def __call__(self, hit):
        hit_xyz = hit[["x", "y", "z"]]
        through = set([tuple(hit_xyz)]).update(self.through)
        return HelixModuliSpace(through)

    def intersects(self, root_nd):
        b = np.array(list(root_nd))

        # (damien): Can we change the test to support adding more hits?
        # We don't need this at all for now though.
        assert len(self.through) <= 1

        try:
            hx, hy, hz = self.through.pop()
            self.through.add((hx, hy, hz))

        except KeyError:
            # If no given point, all helixes possible
            return True

        shifts = np.array(list(product(hx - b[0], hy - b[1])))
        c0_extrema = np.array([[b[i, 0] if h <= b[i, 0] else b[i, 1] if h >= b[i, 1] else h
                                for i, h in enumerate((hx, hy))],
                               [b[i, 0] if np.abs(h - b[i, 0]) >= np.abs(h - b[i, 1]) else b[i, 1]
                                for i, h in enumerate((hx, hy))]])

        h0 = np.array([[hx, hy]])
        norms = np.linalg.norm(h0 - c0_extrema, axis=1)

        if (norms[0] < b[2, 1]) or (norms[1] >= b[2, 0]):

            inv_proper_times = b[4]/hz

            angles = np.arctan2(shifts[:, 0],
                                shifts[:, 1])

            omegas = np.outer(angles, inv_proper_times).flatten()
            return any((omegas >= b[3, 0]) & (omegas < b[3, 1]))

        return False

