import sklearn.cluster as sc
from sklearn.preprocessing import StandardScaler

import pandas as pd
import numpy as np

from pgot.propagates import Propagates


class Learner(Propagates):

    def __init__(self, box, integrity_checks=True):
        super().__init__(box, integrity_checks=integrity_checks)

    def fit(self, hits, **kwargs):

        # td: integrity_checks
        return self.do_fit(hits, **kwargs)

    def predict(self, hits, **kwargs):

        self.log("debug", "predicting clusters on %i hits", len(hits))

        if self.integrity_checks:

            self.check(hits)

        clusters = self.do_predict(hits, **kwargs)

        return clusters

    def check(self, hits):

        self.log("debug", "doing integrity checks")

        dup_idx = hits.index.duplicated(keep='first')

        if np.any(dup_idx):

            self.log("critical", "events cannot have duplicated indices.")

            raise ValueError("Events cannot have duplicated indices.")

        self.log("debug", "checks done. Nothing to worry about (probably)")

    def do_fit(self, hits):

        raise NotImplementedError("You need to implement a do_fit method.")

    def do_predict(self, hits):

        raise NotImplementedError("You need to implement a do_predict method.")

    def track_scores_gen(self, with_clusters):

        for cluster_id, cluster_hits in with_clusters.groupby("cluster_id"):
            yield self.track_scoring_fn(cluster_id, cluster_hits)

    def track_scores(self, with_clusters):

        scores_gen = self.track_scores_gen(with_clusters)
        return pd.DataFrame([track for track in scores_gen])

    def fit_predict(self, hits):
        self.fit(hits)

        return self.predict(hits)

    def track_scoring_fn(self, cluster_id, cluster_hits):

        return Learner._double_majority_track_scoring_fn(cluster_id, cluster_hits)

    @classmethod
    def _double_majority_track_scoring_fn(cls, cluster_id, hits):

        truth_dist = hits.groupby("particle_id").count()["hit_id"]
        simple_majority = truth_dist[truth_dist > truth_dist.sum()/2]
        # num_hits_in_prediction = len(hits)

        if not simple_majority.empty:

            the_particle_id = simple_majority.index[0]
            matching_particle_id = the_particle_id

            scoreable_hits = hits[hits["particle_id"] == the_particle_id]
            # num_scoreable_hits = len(scoreable_hits)
            nhits = scoreable_hits["nhits"].iloc[0]

            if truth_dist.loc[the_particle_id] > nhits/2:
                # double majority
                track_score = scoreable_hits["weight"].sum()

            else:
                track_score = 0

        else:
            nhits = np.nan
            # num_scoreable_hits = np.nan
            matching_particle_id = np.nan
            track_score = 0

        return pd.Series({"cluster_id": cluster_id,
                          "matching_id": matching_particle_id,
                          "score": track_score})
# "num_scoreable_hits": num_scoreable_hits,
# "num_hits_in_prediction": num_hits_in_prediction,
# "true_nhits": nhits})


class SklearnWrapper(Learner):

    def __init__(self, box, sklearn_model, *args, **kwargs):
        self.sklearn_model = sklearn_model
        super().__init__(box, *args, **kwargs)

    def preprocess(self, hits):

        raise NotImplementedError("You need to define a preprocess method.")

    def do_predict(self, event_hits):

        if event_hits.empty:
            return pd.DataFrame([], columns=["cluster_id"])

        X = self.preprocess(event_hits)
        cluster_ids = self.sklearn_model.fit_predict(X)

        return pd.DataFrame(cluster_ids,
                            index=event_hits.index,
                            columns=["cluster_id"])


class DBSCAN(SklearnWrapper):

    def __init__(self, box, eps, min_samples,
                 *args, **kwargs):

        dbscan = sc.DBSCAN(eps, min_samples,
                           algorithm='kd_tree')
        super().__init__(box, sklearn_model=dbscan,
                         *args, **kwargs)

    def propagate(self, box, **kwargs):

        return super().propagate(box, eps=self.eps,
                                 min_samples=self.min_samples,
                                 **kwargs)

    def preprocess(self, hits):

        hits = pd.DataFrame(hits)
        x, y, z = hits.x.values, hits.y.values, hits.z.values
        r = np.sqrt(x**2 + y**2 + z**2)
        hits["x2"] = x/r
        hits["y2"] = y/r

        r = np.sqrt(x**2 + y**2)
        hits["z2"] = z/r

        ss = StandardScaler()
        X = ss.fit_transform(hits[["x2", "y2", "z2"]].values)

        return X
