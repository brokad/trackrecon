import numpy as np


class LinesModuliSpace:

    @classmethod
    def useTF(cls):
        import tensorflow as tf

        ((in_theta_lh, in_r_lh), in_pts), mask = cls.TF_Mask()

        cls._TF_Mask_Graph = mask

        def intersections(cls, box, hits):

            pts = hits[["arcxy", "z"]].values
            theta_lh, r_lh = box

            with tf.Session() as sess:

                mask = sess.run(cls._TF_Mask_Graph,
                                feed_dict={in_theta_lh: theta_lh,
                                           in_r_lh: r_lh,
                                           in_pts: pts})

            return mask

        cls.intersections = intersections

    @classmethod
    def intersections(cls, box, hits):

        mask = []

        for _, pt in hits.iterrows():

            (theta_l, theta_h), (r_l, r_h) = box

            x0, y0 = pt[["arcxy", "z"]]

            rtheta_l = x0*np.cos(theta_l) + y0*np.sin(theta_l)

            if rtheta_l < r_h and rtheta_l >= r_l:
                mask += [True]
                continue

            else:

                rtheta_h = x0*np.cos(theta_h) + y0*np.sin(theta_h)

                if rtheta_h < r_h and rtheta_h >= r_l:
                    mask += [True]
                    continue

                else:
                    mask += [False]
                    continue

        return mask

    @classmethod
    def TF_Mask(cls):
        import tensorflow as tf

        theta_lh = tf.placeholder(tf.float32, shape=[2, 1])
        r_lh = tf.placeholder(tf.float32, shape=[2, 1])

        pts = tf.placeholder(tf.float32, shape=[None, 2])

        pivot_mat = tf.concat([tf.cos(theta_lh), tf.sin(theta_lh)],
                              axis=1)

        rthetas = tf.matmul(pts, pivot_mat, transpose_b=True)

        between_rs = tf.logical_and(rthetas >= r_lh[0],
                                    rthetas < r_lh[1])

        mask = tf.logical_or(between_rs[:, 0],
                             between_rs[:, 1])

        return ((theta_lh, r_lh), pts), mask
