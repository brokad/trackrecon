import pandas as pd


def delta_score_fn(orig, stages):

    orig_t = orig["score"].sum()
    stages_d = pd.Series([stage["score"].sum() - orig_t
                          for stage in stages])
    return stages_d
