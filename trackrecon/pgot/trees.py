import numpy as np
import pandas as pd

from functools import partial
from itertools import chain

from pgot.learner import Learner
from pgot.binning import Bin


MAXUINT64 = np.iinfo(np.uint64).max


class LeafMap:

    def __init__(self, mapFn):

        self._internal = mapFn

    def __call__(self, tree, hits):

        return self._internal(tree, hits)

    @classmethod
    def Self(cls, leaf, hits):

        return leaf

    @classmethod
    def Identity(cls, leaf, hits):

        return hits

    @classmethod
    def Fit(cls, leaf, hits):

        return leaf.learner.fit(hits)


class LeafCombine:

    def __init__(self, combineFn):
        '''
        create an impure associative reducer out of an
        arbitrary conservative and associative function `combineFn`.
        '''
        self._internal = combineFn

    def __call__(self, tree, left, right):

        ls, rs = [*left], [*right]

        if not ls:

            yield from rs

        elif not rs:

            yield from ls

        else:

            assert len(ls) == 1, len(rs) == 1

            ll = ls[0]
            rr = rs[0]

            combined = self._internal(tree, ll, rr)

            assert not combined.empty

            yield combined

    @classmethod
    def Chain(cls, tree, l, r):

        yield from chain(l, r)


class PartitionTree:

    def __init__(self, box, hough_map):

        self.hough_map = hough_map

        self.box = Bin(box)
        self.left = None
        self.right = None

        self._cache = dict()

        self.staging = []

    def getNode(self, address, cache=False, last_split_depth=1):

        if not isinstance(address, str):

            return address

        if not address:

            return self

        try:

            assert last_split_depth >= 0

            len_before_split = len(address)-last_split_depth
            prefix_address = address[:len_before_split]
            relative_address = address[len_before_split:]

            relative_root = self._cache[prefix_address]

            return relative_root.getNode(relative_address,
                                         cache=False,
                                         last_split_depth=0)

        except KeyError:

            n = address[0]

            if n == "l":

                next_node = self.left

            elif n == "r":

                next_node = self.right

            if next_node is None:

                raise AttributeError("Node not found.")

            node = next_node.getNode(address[1:], cache=False)

            if cache:

                self._cache[address] = node

        return node

    def isLeaf(self):

        return self.left is None and self.right is None

    def stage(self, tree):

        assert tree not in self.staging
        self.staging += [tree]

    def bifurcate(self, hits):

        assert not self.isLeaf()

        # /!\ This assumes all hits traverse self
        l_hits, nl_hits = self.left.filter(hits)
        r_hits, _ = self.right.filter(l_hits)

        return l_hits, pd.concat([nl_hits, r_hits], copy=False)

    def commit(self, i):

        # We never replace committed leaves with new ones
        assert self.isLeaf()

        self.append(self.staging[i])

        self.staging = []

    def append(self, other):

        self.left, self.right = other

    def leaves(self, hits=None, with_staging=False,
               as_enumerate=False,
               skip_self=False):

        yield from self._mapcombine(hits=hits, mapFn=LeafMap.Self,
                                    with_staging=with_staging,
                                    as_enumerate=as_enumerate,
                                    skip_self=skip_self)

    def map(self, fn, hits,
            with_staging=False,
            as_enumerate=False,
            skip_self=False):

        yield from self._mapcombine(hits=hits, mapFn=fn,
                                    combineFn=LeafCombine.Chain,
                                    with_staging=with_staging,
                                    as_enumerate=as_enumerate,
                                    skip_self=skip_self)

    def reduce(self, mapFn, hits,
               combineFn,
               with_staging=False,
               as_enumerate=False,
               skip_self=False):

        yield from self._mapcombine(hits=hits, mapFn=mapFn,
                                    combineFn=combineFn,
                                    with_staging=with_staging,
                                    as_enumerate=as_enumerate,
                                    skip_self=skip_self)

    def _mapcombine(self, hits=None,
                    mapFn=LeafMap.Identity,
                    combineFn=LeafCombine.Chain,
                    with_staging=False,
                    as_enumerate=False,
                    skip_self=False):

        if hits is not None:

            kept, _ = self.filter(hits)

            if kept.empty:

                return

        else:

            kept = None

        kwargs = {
            "hits": kept,
            "mapFn": mapFn,
            "combineFn": combineFn,
            "with_staging": with_staging,
            "as_enumerate": as_enumerate
        }

        if as_enumerate:

            wrapleft = lambda l: map(makeleft, l)
            wrapright = lambda r: map(makeright, r)
            wrapstage = lambda l, r: map(partial(makestage, i), combineFn(self, l, r))

        else:

            wrapleft = lambda l: l
            wrapright = lambda r: r
            wrapstage = lambda l, r: combineFn(self, l, r)

        if self.isLeaf():

            if not skip_self:

                if as_enumerate:

                    yield ("", mapFn(self, kept))

                else:

                    yield mapFn(self, kept)

            if with_staging:

                for i, (left, right) in enumerate(self.staging):

                    yield from wrapstage(wrapleft(left._mapcombine(**kwargs)),
                                         wrapright(right._mapcombine(**kwargs)))

        else:

            yield from combineFn(self,
                                 wrapleft(self.left._mapcombine(**kwargs)),
                                 wrapright(self.right._mapcombine(**kwargs)))

    def getNumStages(self):

        return len(self.staging)

    def __iter__(self):

        yield from (self.left, self.right)

    def _filter(self, hits):

        return self.houghMap.intersections(self.box, hits)

    def filter(self, hits):

        if hits.empty:

            return hits, hits

        else:

            crossing = self._filter(hits)

            return hits[crossing], hits[~crossing]

    def __contains__(self, hit):

        return self.hough_map(hit).intersects(self.box)


class HoughTreeEnsemble(PartitionTree, Learner):

    def __init__(self, learner, strategy, box,
                 hough_map, integrity_checks=True):

        self.learner = learner
        self.strategy = strategy

        PartitionTree.__init__(self, box, hough_map)
        Learner.__init__(self, box, integrity_checks=integrity_checks)

        self.f_mapFn = LeafMap.Fit

        self.p_mapFn = LeafMap(HoughTreeEnsemble.predictMapFn)
        self.p_combineFn = LeafCombine(HoughTreeEnsemble.predictCombineFn)

    @classmethod
    def predictCombineFn(cls, tree, left, right):

        return tree.strategy.predict_ensemble(left, right)

    @classmethod
    def predictMapFn(cls, tree, hits):

        clusters = tree.learner.predict(hits)

        cluster_ids = clusters["cluster_id"].unique()

        randomized = {c_id:
                      (np.random.randint(1, MAXUINT64,
                                         dtype=np.uint64)) if c_id != -1 else 0
                      for c_id in cluster_ids}

        clusters["cluster_id"] = clusters["cluster_id"]\
            .apply(randomized.__getitem__)

        return clusters

    def withSplitAt(self, at):

        assert self.isLeaf()

        left_b, right_b = self.box.splitAt(at)

        tree = self(self.box)  # propagate self first

        # then propagate children of the staged tree
        tree.left, tree.right = tree(left_b), tree(right_b)

        return tree

    def propagate(self, box):

        learner = self.learner(box)
        strategy = self.strategy(box)

        return self.__class__(learner, strategy,
                              box, self.hough_map,
                              integrity_checks=self.integrity_checks)

    def append(self, tree):

        try:

            self.learner = tree.learner
            self.strategy = tree.strategy

        except AttributeError:

            raise ValueError("Can only append subtrees "
                             "with learner and strategy breeds")

        super().append(tree)

    def do_fit(self, hits, fit_stages=True):

        consumed = list(self.map(fn=self.f_mapFn, hits=hits,
                                 with_staging=fit_stages))

        return consumed

    def do_predict(self, hits):

        consumed = list(self.reduce(mapFn=self.p_mapFn, hits=hits,
                                    combineFn=self.p_combineFn))

        return consumed[0]


def makeleft(elt):

    addr, e = elt

    return ("l" + addr, e)


def makeright(elt):

    addr, e = elt

    return ("r" + addr, e)


def makestage(i, elt):

    addr, e = elt

    return ("", (i, elt))
