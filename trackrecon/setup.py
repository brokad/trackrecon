from setuptools import setup, find_packages

try:
    from pip._internal.req import parse_requirements
except ImportError:
    from pip.req import parse_requirements

reqs = parse_requirements('requirements.txt', session=False)
requirements = [str(ir.req) for ir in reqs]

setup(
    name="trackrecon",
    version="0.1.dev",
    install_requires=requirements,
    packages=find_packages()
)
