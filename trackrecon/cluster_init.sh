#!/bin/bash

# YOLO.
sed -i -e 's/ \(stable\|jessie\)/ testing/ig' /etc/apt/sources.list

/usr/bin/apt-get -y update
DEBIAN_FRONTEND=noninteractive /usr/bin/apt-get -y install libc6-dev
DEBIAN_FRONTEND=noninteractive /usr/bin/apt-get -y install python3

/usr/bin/apt-get -y install \
		 python3-pip \
		 python3-numpy \
		 python3-scipy \
		 python3-pandas \
		 python3-sklearn

yes | /usr/bin/pip3 install \
		    pyarrow \
		    google.cloud

